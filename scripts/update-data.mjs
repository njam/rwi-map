import {parse as parseCsv} from 'csv-parse/sync'
import axios from 'axios'
import fs from 'fs'

async function main() {
  // Config
  let url = `https://docs.google.com/spreadsheets/d/1G4sT4GgLOD3_4Gz0z0EiN0KRH3MYFWNNkLehRiPLayc/gviz/tq?tqx=out:csv&sheet=Sheet1`;
  let outputPath = './src/assets/map/pois.json'

  // Download and parse CSV
  let response = await axios.get(url);
  let csvRows = parseCsv(response.data, {
    columns: true, skip_empty_lines: true
  });

  // Convert to geojson
  let features = csvRows.reduce(function (acc, row) {
    let feature = convertRowToFeature(new CsvRow(row))
    if (feature !== null) {
      acc.push(feature);
    }
    return acc;
  }, []);
  let geojson = {
    'type': 'FeatureCollection', 'features': features,
  };

  // Write to file
  let geojsonText = JSON.stringify(geojson, null, 2);
  await fs.writeFile(outputPath, geojsonText, (err) => {
    if (err !== null) {
      throw new Error(`Cannot write file "${outputPath}": ${err}`);
    }
  });
}


// Returns either a GeoJSON feature, or `null` if the row should be ignored.
function convertRowToFeature(row) {
  if (row.coords === null) {
    return null;
  }
  return {
    'type': 'Feature',
    'properties': row,
    'geometry': {
      'type': 'Point', 'coordinates': [row.coords[1], row.coords[0]],
    }
  }
}

// Parse coordinates of form "45.46,9.18" and return as array
function parseCoordinates(coords) {
  let pattern = /^([\d.]+)\s*,\s*([\d.]+)$/;
  let result = coords.match(pattern);
  if (result === null) {
    throw new Error(`Cannot extract coords with pattern "${pattern}" from "${coords}"`);
  }
  return [result[1], result[2]]
}

// Parse the columns in the CSV
class CsvRow {
  constructor(row) {
    function get(row, columnName) {
      if (!(columnName in row)) {
        throw new Error(`Missing column "${columnName}": ${JSON.stringify(row)}`);
      }
      let value = row[columnName];
      value = value.trim();
      if (value.length === 0) {
        value = null;
      }
      return value;
    }

    function getService(row, columnName) {
      let text = get(row, columnName)
      return text === 'x'
    }

    function getPhone(row, columnName) {
      let text = get(row, columnName)
      if (text && !text.startsWith('+')) {
        text = `+39 ${text}`;
      }
      return text
    }

    this.name = get(row, 'Nome');
    this.address = get(row, 'Indirizzo');
    this.city = get(row, 'Citta');
    this.openingHoursIt = get(row, 'Orari (IT)');
    this.openingHoursEn = get(row, 'Orari (EN)');
    this.openingHoursUk = get(row, 'Orari (UK)');
    this.email = get(row, 'Email');
    this.phone1 = getPhone(row, 'Telefono 1');
    this.phone2 = getPhone(row, 'Telefono 2');
    this.website = get(row, 'Sito web');
    this.descriptionIt = get(row, 'Descrizione (IT)');
    this.descriptionEn = get(row, 'Descrizione (EN)');
    this.descriptionUk = get(row, 'Descrizione (UK)');

    let coords = get(row, 'Coordinate');
    if (coords !== null) {
      coords = parseCoordinates(coords);
    }
    this.coords = coords;


    let services = {
      'support_primary': getService(row, 'Servizio: Supporto primario'),
      'support_legal': getService(row, 'Servizio: Supporto legale'),
      'support_psychological': getService(row, 'Servizio: Supporto psicologico'),
      'medical': getService(row, 'Servizio: Assistenza sanitaria'),
      'children': getService(row, 'Servizio: Spazi per bambini e ragazzi'),
      'italian_courses_for_children': getService(row, 'Servizio: Corso di italiano per ragazzi'),
      'italian_courses_for_adults': getService(row, 'Servizio: Corso di italiano per adulti'),
      'socialize': getService(row, 'Servizio: Spazi di socializzazione'),
      'sport': getService(row, 'Servizio: Sport'),
      'disabilities': getService(row, 'Servizio: Opportunità per persone con disabilità'),
    };
    this.services = Object.entries(services)
      .filter(([_k, v]) => v)
      .map(([k, _v]) => k);
  }
}


await main();
