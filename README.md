rwi-map
=======

[www.milano-emergenza-ucraina.it](https://www.milano-emergenza-ucraina.it) -  Oppurtunità per rifugiati ucraini


Development
-----------

This website is built usind the [Nuxt.js](https://nuxtjs.org/) web framework and the [MapLibre](https://maplibre.org/) mapping library.

Install dependencies:

```sh
yarn install
```

Serve in dev mode at http://localhost:3000

```sh
yarn run dev
```

### Try out static/generated version

Generate the static version:

```
yarn run generate
```

Then serve at http://localhost:3000:

```
yarn run serve
```


Database - Spreadsheet
----------------------

The data about which places to display on the map, and the description of those places is stored in this Google spreadsheet:  
https://docs.google.com/spreadsheets/d/1G4sT4GgLOD3_4Gz0z0EiN0KRH3MYFWNNkLehRiPLayc

Using the "update-data" command the spreadsheet is downloaded and stored locally in a JSON file:

```
yarn update-data
```

### Menu entry to deploy to production

A Google "Apps Script" is installed, which adds a menu entry to the spreadsheet to publish the data to production.
It uses the following code:
```js
function onOpen() {
  SpreadsheetApp.getUi()
    .createMenu('⬆ Publish')
    .addItem('Publish to "milano-emergenza-ucraina.it"', 'publishData')
    .addToUi();
}

function publishData() {
  // Trigger Vercel build
  // See https://vercel.com/njam/rwi-map/settings/git
  var url = "https://api.vercel.com/v1/integrations/deploy/prj_NHw5l8LqbcqHgznFMK7omCChquJp/b1tvUpbPOS";
  Logger.log("Sending POST request: " + url);
  
  var response = UrlFetchApp.fetch(url, {"method": "post"}); 
  var responseCode = response.getResponseCode();
  var responseText = response.getContentText();
  var responseSummary = "Response code = " + responseCode + "\n Response text = " + responseText;
  Logger.log("Received response: \n\n" + responseSummary);

  if (responseCode != 201) {
    SpreadsheetApp.getUi().alert("Request failed!\n\n " + responseSummary);
  }
}
```


Deployment
----------

This project is deployed at [Vercel.com](https://vercel.com/njam/rwi-map).
The build is configured to update the data, and generate the static version of the site (`yarn run update-data && yarn run generate`).

A deployment is _automatically_ triggered when:
- The "publish" menu entry in the spreadsheet is clicked (see above)
- Code changes are pushed to Gitlab (via [Vercel integration](https://vercel.com/njam/rwi-map)).
- Every 24 hours (via [periodic Gitlab CI job](https://gitlab.com/njam/rwi-map/pipeline_schedules)).

To _manually_ deploy to Vercel from local code:
```
yarn run deploy
```
