export default {
  srcDir: 'src',
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head() {
    const i18nHead = this.$nuxtI18nHead({addSeoAttributes: true})
    return {
      title: this.$t('meta.title', {name: this.$t('name'), description: this.$t('meta.description')}),
      htmlAttrs: {
        ...i18nHead.htmlAttrs
      },
      meta: [
        {charset: 'utf-8'},
        {name: 'viewport', content: 'width=device-width, initial-scale=1'},
        {hid: 'description', name: 'description', content: this.$t('meta.description')},
        ...i18nHead.meta
      ],
      link: [
        // Specify vuetify dependencies manually, due to https://github.com/nuxt-community/vuetify-module/issues/340
        // TODO: consider inlining these deps
        {
          rel: 'stylesheet',
          type: "text/css",
          href: "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;display=swap"
        },
        {
          rel: 'stylesheet',
          type: "text/css",
          href: "https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css"
        },
        ...i18nHead.link
      ]
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/css/main.scss"
  ],
  styleResources: {
    scss: [
      '~/assets/scss/variables.scss'
    ]
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    ['@nuxtjs/vuetify', {
      optionsPath: './vuetify.options.js'
    }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    ['@nuxtjs/i18n', {
      locales: [
        {
          code: 'it',
          iso: 'it-IT',
          name: 'Italiano',
          file: 'it.json'
        },
        {
          code: 'en',
          iso: 'en-US',
          name: 'English',
          file: 'en.json'
        },
        {
          code: 'uk',
          iso: 'uk-UA',
          name: 'Українська',
          file: 'uk.json'
        }
      ],
      defaultLocale: 'it',
      strategy: 'prefix',
      detectBrowserLanguage: {
        useCookie: false
      },
      langDir: 'translation/',
      baseUrl: 'https://www.milano-emergenza-ucraina.it',
      vueI18n: {
        fallbackLocale: 'it'
      }
    }]
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  generate: {
    // Don't generate a fallback page (200.html), because all routes are generated for this app.
    fallback: false
  }
}
